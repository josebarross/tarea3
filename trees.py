from random import sample, choice, random


class Node:
    def __init__(self, l, value, r):
        self.l = l
        self.value = value
        self.r = r

    def __str__(self):
        """String print of node"""
        return "Node({l} {v} {r})".format(l=self.l, v=self.value, r=self.r)

    def depth(self):
        """Returns depth of node"""
        if type(self.l) != Node and type(self.r) != Node:
            return 2
        elif type(self.l) != Node and type(self.r) == Node:
            return 1 + self.r.depth()
        elif type(self.l) == Node and type(self.r) != Node:
            return 1 + self.l.depth()
        else:
            return 1 + max(self.l.depth(), self.r.depth())

    def list_of_nodes(self):
        """Returns this and all recursive childs as a list"""
        nodes = [self]
        if type(self.l) == Node:
            nodes += self.l.list_of_nodes()
        if type(self.r) == Node:
            nodes += self.r.list_of_nodes()
        return nodes

    def crossover_point(self, max_depth=False):
        """Selects a random child or itself as a crossover point
        if max_depth selected has to be equall or less than max_depth"""
        if max_depth:
            return choice([x for x in self.list_of_nodes() if x.depth() <= max_depth])
        return choice(self.list_of_nodes())

    def copy(self):
        """Returns a new node copid from this one, hard copy"""
        if type(self.l) == Node:
            l = self.l.copy()
        else:
            l = self.l
        if type(self.r) == Node:
            r = self.r.copy()
        else:
            r = self.r
        return Node(l, self.value, r)

    def update(self, node):
        """"Updates the values of this node with another nodes childs and values"""
        self.l = node.l
        self.value = node.value
        self.r = node.r


def random_node(function_set, terminal_set, depth, prob_of_leaf=0.3):
    """Construct a random node of depth, with inner nodes in function_set and leafs in terminal_set
    prob_of_leaf is the probability that one of childs of inner node is leaf"""
    if depth == 2:
        return Node(choice(terminal_set), choice(function_set), choice(terminal_set))
    elif random() < prob_of_leaf:
        node = random_node(function_set, terminal_set, depth - 1, prob_of_leaf)
        if random() < 0.5:
            return Node(node, choice(function_set), choice(terminal_set))
        else:
            return Node(choice(terminal_set), choice(function_set), node)
    else:
        node1 = random_node(function_set, terminal_set, depth - 1, prob_of_leaf)
        node2 = random_node(function_set, terminal_set, depth - 1, prob_of_leaf)
        return Node(node1, choice(function_set), node2)


def eval_node(node, params={}):
    """Evaluates node, params is a parametrization ex: (X=2)"""
    if type(node) == Node:
        if node.value == "+":
            return eval_node(node.l, params) + eval_node(node.r, params)
        elif node.value == "-":
            return eval_node(node.l, params) - eval_node(node.r, params)
        elif node.value == "/":
            return eval_node(node.l, params) / eval_node(node.r, params)
        elif node.value == "*":
            return eval_node(node.l, params) * eval_node(node.r, params)
    elif type(node) == int or type(node) == float:
        return node
    else:
        return params[node]
