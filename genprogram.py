from abc import ABC, abstractmethod
import statistics
import random
import string
import matplotlib.pyplot as plt
from trees import eval_node, random_node


class GeneticProgram(ABC):
    def __init__(
        self,
        nprograms,
        functional_set,
        terminal_set,
        target,
        max_depth,
        prob_mutation=0.2,
        selection_method="tournament",
        verbose=True,
        n_tournament=15,
    ):
        """nprograms number of programs in the set
        functional set, functions that can be in inner nodes (usually +, -, *, /)
        terminal_set, numbers or variables in leafs
        target the target result oof the program, to be used in fitness function
        max_depth the max depth of the program
        prob_mutation the probabilit of a program getting mutated
        be either roulette or tournament, verbose stores fitness data across loops and n_tournament is the
        amount of programs selected for tournament"""
        self.nprograms = nprograms
        self.max_depth = max_depth
        depths = [random.choice(range(2, max_depth + 1)) for i in range(nprograms)]
        self.fs = functional_set
        self.ts = terminal_set
        self.programs = [random_node(self.fs, self.ts, i) for i in depths]
        self.target = target
        self.mean_error = []
        self.min_error = []
        self.verbose = verbose
        self.n_tournament = n_tournament
        self.selection_method = selection_method
        self.best_program = None
        self.prob_mutation = prob_mutation

    def run(self, use_epochs=False, epochs=500):
        """Runs the Genetic Program (GP) from now on, and all its stages."""
        i = 1
        self.best_fitness = float("-inf")
        while i < epochs or not use_epochs:
            if i % epochs == 0:
                self.print_execution_info(i)
            i += 1
            self.evaluate_fitness()
            if self.result_found():
                break
            if epochs - i < 10:
                self.get_best_program()
            if self.verbose:
                self.mean_error.append(statistics.mean(self.error()))
                self.min_error.append(min(self.error()))
            self.selection()
            self.reproduction()
        self.evaluate_fitness()
        self.get_best_program()

    def print_execution_info(self, i):
        """Prints execution info"""
        self.get_best_program()
        bp = self.get_best_program(evaluate=True)
        print(f"Epoch {i}: Best Equation: {bp}")

    def evaluate_fitness(self):
        """Calculates fitness for every object"""
        self.fitness = [self.fitness_f(s) for s in self.programs]

    def selection(self):
        """Runs the method of selection given"""
        if self.selection_method == "roulette":
            self.selected = self.roulette()
        else:
            self.selected = self.tournament()

    def reproduction(self):
        """Does a crossover and then a mutation"""
        self.crossover()
        self.mutation()

    def roulette(self):
        """Implements roulette selection"""
        sum_sub = sum(self.fitness)
        r = random.uniform(0, sum_sub)
        for i, fit in enumerate(self.fitness):
            if r <= fit:
                return self.programs[i - 1]
            r -= fit

    def tournament(self):
        """Implements a tournament selection"""
        sel_index = random.sample(range(len(self.programs)), self.n_tournament)
        max_fit = float("-inf")
        for i in sel_index:
            if self.fitness[i] > max_fit:
                max_fit = self.fitness[i]
                max_sub = self.programs[i]
        return max_sub

    def get_best_program(self, evaluate=False):
        """if evaluate=True returns the best program of last 10 epochs
        else calculates the best program and if it is better than the one of
        the other epochs stores it in self.best_program
        This method is called only the last 10 epochs"""
        if evaluate:
            return self.best_program
        max_fitness = self.fitness[0]
        best_program = self.programs[0]
        for f, s in zip(self.fitness, self.programs):
            if f > max_fitness:
                max_fitness = f
                best_program = s
        if max_fitness > self.best_fitness:
            self.best_fitness = max_fitness
            self.best_program = best_program

    def plot(self, n, algorithm, separated=False):
        """Plots mean and min error, eliminates outliers so it can be easier to see"""
        plt.figure(n)
        plt.title("Error vs Loop in {}".format(algorithm))
        plt.ylabel("Error")
        plt.xlabel("Loop")
        mmin = statistics.mean(self.min_error)
        min_error = [e for e in self.min_error if abs(e - mmin) < 2 * mmin]
        mmean = statistics.mean(self.mean_error)
        mean_error = [e for e in self.mean_error if abs(e - mmean) < 2 * mmean]
        plt.plot(min_error, label="Minimum error of subjects")
        if not separated:
            plt.plot(mean_error, label="Mean error of subjects")
        plt.legend()
        plt.savefig("{}.png".format(algorithm))
        if separated:
            plt.figure(n + 1)
            plt.title("Error vs Loop in {}".format(algorithm))
            plt.ylabel("Error")
            plt.xlabel("Loop")
            plt.plot(mean_error, label="Mean error of subjects")
            plt.legend()
            plt.savefig("{}-mean.png".format(algorithm))

    def crossover(self):
        """Does a crossover between the selected program and all others, stores the selected as it is"""
        self.programs.remove(self.selected)
        next_generation = []
        for p in self.programs:
            next_generation.append(self.crossover_program(p))
        next_generation.append(self.selected)
        self.programs = next_generation

    def crossover_program(self, program):
        """Crossover between selected program and a single program"""
        new_program = self.selected.copy()
        cross1 = new_program.crossover_point()
        max_depth = max(2, self.max_depth - (new_program.depth() - cross1.depth()) + 1)
        cross2 = program.copy().crossover_point(max_depth)
        cross1.update(cross2)
        return new_program

    def mutation(self):
        """Mutates every program in generation with prob=self.prob_mutation"""
        for i, p in enumerate(self.programs):
            if random.random() < self.prob_mutation:
                new_p = p.copy()
                mut_p = new_p.crossover_point()
                max_depth = max(2, self.max_depth - (new_p.depth() - mut_p.depth()) + 1)
                depth = max(2, random.choice(range(max_depth)))
                mut_p.update(random_node(self.fs, self.ts, depth))
                self.programs[i] = mut_p

    @abstractmethod
    def fitness_f(self, subject):
        pass

    @abstractmethod
    def error(self):
        pass

    @abstractmethod
    def result_found(self):
        pass


class TargetNumberGP(GeneticProgram):
    def __init__(
        self,
        target,
        terminal_set,
        nprograms=20,
        max_depth=5,
        functional_set=["+", "-", "*", "/"],
    ):
        """Calls super init with default max_depth=5 and default n_programs=20"""
        super().__init__(nprograms, functional_set, terminal_set, target, max_depth)

    def result_found(self):
        if max(self.fitness) == float("inf"):
            return True
        return False

    def fitness_f(self, program):
        """Returns inverse of distance to target"""
        try:
            d = abs(eval_node(program) - self.target)
            if d == 0:
                return float("inf")
            return 1.0 / d
        except:
            return 0

    def error(self):
        """Returns distance to target"""
        errors = []
        for p in self.programs:
            try:
                errors.append(abs(eval_node(p) - self.target))
            except:
                errors.append(self.target)
        return errors


class FitFunctionGP(GeneticProgram):
    def __init__(
        self,
        target,
        terminal_set,
        nprograms=100,
        max_depth=15,
        functional_set=["+", "-", "*", "/"],
    ):
        """Calls super init with default max_depth=20 and default n_programs=50"""
        super().__init__(nprograms, functional_set, terminal_set, target, max_depth)

    def print_execution_info(self, i):
        super().print_execution_info(i)
        for x, y in self.target:
            bp = self.get_best_program(evaluate=True)
            print(
                "Equation Result: x={}, f(x)={}, y={}".format(x, eval_node(bp, {"x": x}), y)
            )

    def result_found(self):
        if max(self.fitness) >= 1.0 / 0.1:
            return True
        return False

    def fitness_f(self, program):
        """Return inverse of the max of distances between the target function results and the result of the program to the same x"""
        try:
            distances = [abs(y - eval_node(program, {"x": x})) for x, y in self.target]
            d = max(distances)
            if d == 0:
                return float("inf")
            return 1.0 / d
        except:
            return 0

    def error(self):
        """Returns sum of distances to points"""
        errors = []
        for p in self.programs:
            try:
                distances = [abs(y - eval_node(p, {"x": x})) for x, y in self.target]
                errors.append(sum(distances))
            except:
                errors.append(sum([y for x, y in self.target]))
        return errors


def target_number_experiment():
    """Runs target number genetic program and prints results and plots mean and min error"""
    terminal_set = [25, 7, 8, 100, 4, 2]
    target = 432
    target_number = TargetNumberGP(target, terminal_set)
    target_number.run()
    target_number.plot(1, "target number genetic program", separated=True)
    bp = target_number.get_best_program(evaluate=True)
    print("Best Equation: {}".format(bp))
    print("Equation Result: {}, Target {}\n".format(eval_node(bp), target))


def fit_function_experiment():
    """Runs fit function genetic program and prints results and plots mean and min error"""
    terminal_set = 100 * ["x"] + [x / 10.0 for x in range(1, 100)]
    # Repeat x 50  times to boost its probs of being choosen
    target = [(3, 4), (6, 2), (7, 5)]
    ff = FitFunctionGP(target, terminal_set)
    ff.run()
    ff.plot(3, "fit function genetic program", separated=True)
    bp = ff.get_best_program(evaluate=True)
    print("Best Equation: {}".format(bp))
    for x, y in target:
        print(
            "Equation Result: x={}, f(x)={}, y={}".format(x, eval_node(bp, {"x": x}), y)
        )
    print()


print("Target number experiment\n")
target_number_experiment()
print("\n")
print("Fit Function Experiment\n")
fit_function_experiment()
