# Install Instructions
To run first install python. This was run with Python 3.8.
 - Create and activate virtual enviroment
 - Install dependencies (I matplotlib with its dependencies):
 `pip install -r requirements.txt`
 - Run:
 `python genprogram.py`
 
 # Code structure
 - genprogram.py has the implementation of the genetic programs and the functions that runs the experiment. Also it runs the experiments.
 - trees.py has the implementation of the Node class and the functions random\_node and eval\_node.
